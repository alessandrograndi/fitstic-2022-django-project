# from rest_framework import generics
from rest_framework.viewsets import ModelViewSet

from library.models import Book
from library_api_v1.serializers import BookSerializer


class BookViewSet(ModelViewSet):
    queryset = Book.objects.all()
    serializer_class = BookSerializer


# class BookListAPIView(generics.ListAPIView):
#     queryset = Book.objects.all()
#     serializer_class = BookSerializer
