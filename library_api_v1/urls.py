from django.urls import path
from rest_framework.routers import SimpleRouter

from library_api_v1 import api_views

app_name = 'library_api_v1'

book_router = SimpleRouter()

book_router.register(
    'libri',
    api_views.BookViewSet,
    basename="books"
)

urlpatterns = book_router.urls

# urlpatterns = [
#     path(
#         'libri/',
#         api_views.BookListAPIView.as_view(),
#         name="book_list"
#     )
# ]
