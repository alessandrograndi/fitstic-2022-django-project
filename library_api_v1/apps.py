from django.apps import AppConfig


class LibraryApiV1Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'library_api_v1'
