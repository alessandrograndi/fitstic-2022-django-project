from unittest import TestCase

from tools.tests.mixins import TestUserMixin, TestClientMixin

__all__ = ['GenericTestCase']


class GenericTestCase(
    TestUserMixin,
    TestClientMixin,
    TestCase,
):
    pass
