from django.contrib.auth import get_user_model
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import Client

__all__ = ["TestUserMixin", "TestClientMixin"]


def new_user(username, password):
    user = get_user_model().objects.create(username=username)
    user.set_password(password)
    user.save()
    return user


class TestUserMixin:
    @staticmethod
    def create_user(*args, **kwargs):
        user = new_user(*args, **kwargs)
        return user


class TestClientMixin:
    @staticmethod
    def login_user(username, password, **kwargs):
        client = Client()
        client.login(username=username, password=password)
        return client

    @staticmethod
    def get_a_simple_jpg():
        return SimpleUploadedFile(
            "test_file.jpg", b"file_content", content_type="image/jpg"
        )
