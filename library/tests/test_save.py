from django.test import TestCase

from library.models import Book


class MyFirstTestCase(TestCase):

    def test_save(self):
        b = Book(name="ABC")
        self.assertIs(None, b.pk)

        b.save()
        self.assertEqual(1, b.pk)

        self.assertEqual(b, Book.objects.first())


class UpdateTestCase(TestCase):

    def setUp(self) -> None:
        Book(name="ABC").save()

    def test_update(self):
        b = Book.objects.first()
        self.assertEqual("ABC", b.name)

        b.name = "QWERTY"
        b.save()
        self.assertEqual("QWERTY", b.name)
        self.assertEqual("QWERTY", Book.objects.first().name)
