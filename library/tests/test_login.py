from django.urls import reverse_lazy

from tools.tests.generics import GenericTestCase

__all__ = ["TestLoginAndLogout"]


class TestLoginAndLogout(GenericTestCase):

    user_data = {
        "username": "ALLE",
        "password": "Qwerty123!",
    }

    def setUp(self) -> None:
        self.create_user(**self.user_data)
        self.client = self.login_user(**self.user_data)

    def test_000_login_and_logout(self):

        # Test login
        response = self.client.get(reverse_lazy("home"))
        self.assertEqual(200, response.status_code)
        self.assertIn(
            self.user_data["username"],
            response.content.decode()
        )

        # Test logout
        response = self.client.get(reverse_lazy("logout"))
        self.assertEqual(302, response.status_code)
        self.assertEqual(reverse_lazy("home"), response.url)
