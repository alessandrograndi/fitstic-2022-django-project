from django.urls import path

from library import views

app_name = 'library'

urlpatterns = [
    path(
        'lista-libri/',
        views.BookListView.as_view(),
        name="book_list"
    )
]
