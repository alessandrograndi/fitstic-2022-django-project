from django.views.generic import ListView

from library.models import Book

__all__ = ['BookListView']


class BookListView(ListView):
    model = Book
