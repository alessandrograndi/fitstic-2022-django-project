from django.db import models
from django.utils.text import slugify


def image_path(instance, filename):
    return "images/uploaded/" \
           "{id:05d}-{file_name}.{file_extension}".format(
        id=instance.pk,
        file_name=slugify(instance.name),
        file_extension=filename.split(".")[-1],
    )


class Book(models.Model):
    name = models.CharField(max_length=512)
    description = models.CharField(max_length=1024)
    cover_image = models.ImageField(
        upload_to=image_path
    )

    def save(self, *args, **kwargs):
        if self.pk is None:
            self.description = f"Descrizione di {self.name}"
        super().save(*args, **kwargs)

    def __str__(self):
        return f"Libro: {self.name}"
